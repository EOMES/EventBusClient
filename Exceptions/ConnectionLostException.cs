﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Text;

namespace Oxmes.Core.EventBusClient.Exceptions
{
    public class ConnectionLostException : Exception
    {
        public ConnectionLostException()
        {
        }

        public ConnectionLostException(string message) : base(message)
        {
        }

        public ConnectionLostException(string message, Exception innerException) : base(message, innerException)
        {
        }

        protected ConnectionLostException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}
