﻿using Oxmes.Core.EventBusClient.Enums;
using System;

namespace Oxmes.Core.EventBusClient.Messages
{
    public interface IEventBusMessage
    {
        Guid MessageUuid { get; }

        Guid OriginServiceUuid { get; }

        Severity Severity { get; }

        int MessageType { get; }

        DateTime Time { get; }

        object Payload { get; }
    }
}