﻿using Oxmes.Core.EventBusClient.Enums;

namespace Oxmes.Core.EventBusClient.Messages.Common
{
    public class ServiceShutdownMessage : CommonEventBusMessageBase
    {
        public ServiceShutdownMessage() : base()
        {
            MessageType = (int)CommonMessageTypes.ServiceShutdown;
        }
    }
}