﻿using Oxmes.Core.EventBusClient.Enums;

namespace Oxmes.Core.EventBusClient.Messages.Common
{
    public class SubscribeMessage : CommonEventBusMessageBase
    {
        public new SubscribeMessagePayload Payload
        {
            get => (SubscribeMessagePayload)base.Payload;
            set => base.Payload = value;
        }

        public SubscribeMessage() : base()
        {
            MessageType = (int)CommonMessageTypes.Subscribe;
        }
    }

    public class SubscribeMessagePayload
    {
        public int MinimumSeverity { get; set; } = (int)Severity.Trace;
        public int MaximumSeverity { get; set; } = (int)Severity.Control;
        public int MessageType { get; set; }
    }
}