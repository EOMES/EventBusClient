﻿using Oxmes.Core.EventBusClient.Enums;
using Oxmes.Core.EventBusClient.Messages;
using Oxmes.Core.EventBusClient.Messages.Common;
using System;

namespace Oxmes.Core.EventBusClient
{
    public class EventBusMessageFactory
    {
        public Guid ServiceUuid { get; set; }

        public EventBusMessageFactory()
        {
        }

        public IEventBusMessage CreateEventBusMessage<TMessage>(Severity severity, object payload) where TMessage : CommonEventBusMessageBase, new()
        {
            var msg = new TMessage() { OriginServiceUuid = ServiceUuid, Severity = severity, Payload = payload };
            return msg;
        }

        public IEventBusMessage CreateTraceMessage<TMessage>(object payload) where TMessage : CommonEventBusMessageBase, new()
        {
            return CreateEventBusMessage<TMessage>(Severity.Trace, payload);
        }

        public IEventBusMessage CreateDebugMessage<TMessage>(object payload) where TMessage : CommonEventBusMessageBase, new()
        {
            return CreateEventBusMessage<TMessage>(Severity.Debug, payload);
        }

        public IEventBusMessage CreateInfoMessage<TMessage>(object payload) where TMessage : CommonEventBusMessageBase, new()
        {
            return CreateEventBusMessage<TMessage>(Severity.Info, payload);
        }

        public IEventBusMessage CreateWarnMessage<TMessage>(object payload) where TMessage : CommonEventBusMessageBase, new()
        {
            return CreateEventBusMessage<TMessage>(Severity.Warn, payload);
        }

        public IEventBusMessage CreateErrorMessage<TMessage>(object payload) where TMessage : CommonEventBusMessageBase, new()
        {
            return CreateEventBusMessage<TMessage>(Severity.Error, payload);
        }

        public IEventBusMessage CreateFatalMessage<TMessage>(object payload) where TMessage : CommonEventBusMessageBase, new()
        {
            return CreateEventBusMessage<TMessage>(Severity.Fatal, payload);
        }

        public IEventBusMessage CreateControlMessage<TMessage>(object payload) where TMessage : CommonEventBusMessageBase, new()
        {
            return CreateEventBusMessage<TMessage>(Severity.Control, payload);
        }
    }
}