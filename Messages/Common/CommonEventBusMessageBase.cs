﻿using Oxmes.Core.EventBusClient.Enums;
using System;

namespace Oxmes.Core.EventBusClient.Messages.Common
{
    public abstract class CommonEventBusMessageBase : IEventBusMessage
    {
        public Guid MessageUuid { get; }

        public Guid OriginServiceUuid { get; set; }

        public Severity Severity { get; set; }

        public int MessageType { get; protected set; }

        public DateTime Time { get; set; }

        public virtual object Payload { get; set; }

        public CommonEventBusMessageBase()
        {
            MessageUuid = Guid.NewGuid();
            Time = DateTime.Now;
            MessageType = (int)CommonMessageTypes.Undefined;
        }
    }
}