﻿namespace Oxmes.Core.EventBusClient.Enums
{
    public enum Severity
    {
        Trace = 10,
        Debug = 20,
        Info = 30,
        Warn = 40,
        Error = 50,
        Fatal = 60,

        Control = 1000
    }
}