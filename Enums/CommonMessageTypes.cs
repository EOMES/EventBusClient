﻿namespace Oxmes.Core.EventBusClient.Enums
{
    public enum CommonMessageTypes
    {
        Undefined = 0,
        Subscribe = 10,

        ServiceShutdown = 1000,
    }
}