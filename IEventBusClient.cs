﻿using Oxmes.Core.EventBusClient.Messages;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Oxmes.Core.EventBusClient
{
    public delegate void EventMessageHandler(object sender, EventBusMessageArgs args);

    public interface IEventBusClient
    {
        event EventMessageHandler OnEventMessageReceive;

        string Host { get; set; }
        ushort? Port { get; set; }
        string AuthorizationToken { get; set; }

        Guid ServiceUuid { get; set; }
        bool? Connected { get; }

        IDictionary<int, Type> TypeMap { get; }

        Task<bool> TryConnectAsync();

        Task ConnectAsync();

        void AcceptEvents();

        Task SubscribeAsync(int messageType);

        Task AnnounceShutdownAsync();

        Task AnnounceShutdownAsync(Guid serviceUuid);

        Task SendEventAsync(IEventBusMessage message);

        Task CloseAsync();
    }

    public class EventBusMessageArgs : EventArgs
    {
        public Type MessageType { get; }
        public object Message { get; }

        public EventBusMessageArgs(Type messageType, object message)
        {
            MessageType = messageType;
            Message = message;
        }
    }
}