﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json.Schema;
using System.IO;

namespace Oxmes.Core.EventBusClient
{
    public class MessageValidator
    {
        private static JSchema Schema { get; set; }

        static MessageValidator()
        {
            using (var stream = typeof(MessageValidator).Assembly.GetManifestResourceStream($"Oxmes.Core.EventBusClient.Resources.event-message-json-schema.json"))
            using (var file = new StreamReader(stream))
            using (var jsonReader = new JsonTextReader(file))
            {
                Schema = JSchema.Load(jsonReader);
            }
        }

        public static bool ValidateMessage(string message)
        {
            var jObject = JObject.Parse(message);
            return ValidateMessage(jObject);
        }

        public static bool ValidateMessage(JObject jObject)
        {
            return jObject.IsValid(Schema);
        }
    }
}