﻿using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Oxmes.Core.EventBusClient.Enums;
using Oxmes.Core.EventBusClient.Exceptions;
using Oxmes.Core.EventBusClient.Messages;
using Oxmes.Core.EventBusClient.Messages.Common;
using System;
using System.Collections.Generic;
using System.Net.WebSockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Oxmes.Core.EventBusClient
{
    public class EventBusClient : IEventBusClient
    {
        public event EventMessageHandler OnEventMessageReceive;

        public string Host { get; set; }
        public ushort? Port { get; set; }
        public string AuthorizationToken { get; set; }

        public Guid ServiceUuid { get; set; }

        public bool? Connected => WebSocket?.State == WebSocketState.Open;

        public IDictionary<int, Type> TypeMap { get; } = new Dictionary<int, Type>();

        protected ClientWebSocket WebSocket { get; set; }
        protected Task ListenerTask { get; set; }

        protected readonly ILogger Logger;

        public EventBusClient(ILogger<EventBusClient> logger)
        {
            Logger = logger;
            TypeMap.Add((int)CommonMessageTypes.ServiceShutdown, typeof(ServiceShutdownMessage));
        }

        public async Task<bool> TryConnectAsync()
        {
            if (Host == null) throw new InvalidOperationException(nameof(Host) + " not set");
            if (Port == null) throw new InvalidOperationException(nameof(Port) + " not set");
            if (AuthorizationToken == null) throw new InvalidOperationException(nameof(AuthorizationToken) + " not set");

            WebSocket = new ClientWebSocket();
            WebSocket.Options.SetRequestHeader("Authorization", $"Bearer {AuthorizationToken}");

            try
            {
                await WebSocket.ConnectAsync(new Uri($"ws://{Host}:{Port}"), CancellationToken.None);
            }
            catch (WebSocketException ex)
            {
                Logger.LogInformation("Unable to establish connection to event bus. Reason: " + ex.Message);
                return false;
            }

            return true;
        }

        public async Task ConnectAsync()
        {
            var wait = 0;
            bool success;

            do
            {
                if (wait > 0) Logger.LogInformation($"Waiting {wait / 1000} seconds to retry connecting.");
                Thread.Sleep(wait);
                success = await TryConnectAsync();
                wait = wait > 0 ? Math.Min(wait * 2, 60_000) : 1_000;
            } while (!success);
        }

        public void AcceptEvents()
        {
            ListenerTask = Task.Run(async () =>
            {
                // Create 4 kb buffer
                var buffer = new ArraySegment<byte>(new byte[1024 << 2]);
                while (Connected == true)
                {
                    var data = await WebSocket.ReceiveAsync(buffer, CancellationToken.None);
                    if (data.MessageType == WebSocketMessageType.Text)
                    {
                        var rawString = Encoding.UTF8.GetString(buffer.Array, index: buffer.Offset, count: buffer.Count);
                        var json = JObject.Parse(rawString);
                        var isValid = MessageValidator.ValidateMessage(json);

                        if (isValid)
                        {
                            Logger.LogDebug($"Received message: {json.ToString()}");
                            var messageType = json.Value<int>("MessageType");
                            if (TypeMap.TryGetValue(messageType, out var type))
                            {
                                try
                                {
                                    OnEventMessageReceive?.Invoke(this, new EventBusMessageArgs(type, json.ToObject(type)));
                                }
                                catch (Exception ex)
                                {
                                    Logger.LogError(ex, "Unexpected error");
                                }
                            }
                            else
                            {
                                OnEventMessageReceive?.Invoke(this, new EventBusMessageArgs(typeof(JObject), json));
                            }
                        }
                    }
                }
            });
        }

        public async Task SubscribeAsync(int messageType)
        {
            var factory = new EventBusMessageFactory { ServiceUuid = ServiceUuid };
            var message = factory.CreateControlMessage<SubscribeMessage>(new SubscribeMessagePayload() { MessageType = messageType });

            await SendEventAsync(message);
        }

        public async Task AnnounceShutdownAsync()
        {
            await AnnounceShutdownAsync(ServiceUuid);
        }

        public async Task AnnounceShutdownAsync(Guid serviceUuid)
        {
            var messageFactory = new EventBusMessageFactory { ServiceUuid = serviceUuid };
            await SendEventAsync(messageFactory.CreateControlMessage<ServiceShutdownMessage>(ServiceUuid));
        }

        public async Task SendEventAsync(IEventBusMessage message)
        {
            if (Connected == false) throw new InvalidOperationException("Not connected");
            if (ServiceUuid == null) throw new InvalidOperationException($"{nameof(ServiceUuid)} not set");

            var json = JsonConvert.SerializeObject(message);
            var bytes = Encoding.UTF8.GetBytes(json);
            var buffer = new ArraySegment<byte>(bytes);
            try
            {
                await WebSocket.SendAsync(buffer, WebSocketMessageType.Text, true, CancellationToken.None);
            }
            catch (Exception ex)
            {
                Logger.LogError("Error: {0}", ex);
                throw new ConnectionLostException("Connection to server lost", ex);
            }
        }

        public async Task CloseAsync()
        {
            if (Connected == true)
            {
                await AnnounceShutdownAsync();
                await WebSocket.CloseAsync(WebSocketCloseStatus.NormalClosure, "Shutting Down", CancellationToken.None);
            }
        }
    }
}